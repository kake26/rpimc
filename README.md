# rpimc
Raspberry PI Medial Center

Designed to be used with a google chromecasts. Componets seperated into branches.

# Current Status as of 1/10/22

Allot of recent work on the Frontend. A large part due to having to update the code to a newer version of expo. Backend is ok for the most part a few items to do. See issues for a break down.

# Frontend Branch
A react native android application, acts as a remote control. This is built using
Expo. 

# Backend Branch
A Python based web service and controller chromecasts

# Notes
Example for cloning the frontend
git clone -b frontend https://github.com/kake26/rpimc.git

Some values still hardcoded as its a work in progress. 1.0 Release will be soon.
